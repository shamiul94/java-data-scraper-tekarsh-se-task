import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

// this class handles the data fetching and manipulating part
public class DataHandler {
    // fetches data from the link using Jsoup
    public String fetchData() {
        String html = null;
        try {
            html = Jsoup.connect(Main.url).get().html();
        } catch (IOException e) {
            System.out.println("Could not properly fetch data from given url.");
        }
        return html;
    }

    // extract the header names from the table
    public String[] extractHeaders(Elements wholeTable) {
        Elements headerRow = wholeTable.select("thead").select("tr").select("th");
        if (headerRow.size() == 0) {
            throw new RuntimeException("No Header Data Found.");
        }

        String[] headerList = new String[headerRow.size()];

        for (int i = 0, headerRowSize = headerRow.size(); i < headerRowSize; i++) {
            Element elem = headerRow.get(i);
            headerList[i] = elem.text();
        }
        return headerList;
    }

    // extracts the row data from table
    public ArrayList<String[]> extractDataRows(Elements wholeTable) {
        ArrayList<String[]> dataRowValues = new ArrayList<String[]>();
        Elements dataRowsElement = wholeTable.select("tbody").select("tr");

        // When Table Data Is Empty.
        if (dataRowsElement.size() == 0) {
            throw new RuntimeException("No Data Found.");
        } else {
//            System.out.println(dataRowsElement.size());
            for (Element element : dataRowsElement) {
                Elements dataRow = element.select("td");
                String[] data_list = new String[dataRow.size()];
                for (int i = 0, dataRowSize = dataRow.size(); i < dataRowSize; i++) {
                    Element value = dataRow.get(i);
                    data_list[i] = value.text();
                }
                dataRowValues.add(data_list);
            }
        }
        return dataRowValues;
    }

    // This method parses the html, extracts the table, returns the headers and row data.
    public ArrayList<String[]> parseData(String html) {
        Document doc = Jsoup.parse(html);   // pretty print HTML
        Elements wholeTableElement = doc.select("table");

        ArrayList<String[]> wholeTableAsList = new ArrayList<>();

        // getting the header names
        String[] headersList = extractHeaders(wholeTableElement);
        wholeTableAsList.add(headersList);

        // getting the row data values
        ArrayList<String[]> rowDataLists = extractDataRows(wholeTableElement);
        wholeTableAsList.addAll(rowDataLists);

        return wholeTableAsList;
    }
}
