import com.opencsv.CSVWriter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

// this class uses the data got from datahandler class and write it to csv and returns it to main class
public class Solution {
    // writes a arraylist of string arrays to a csv file
    public void writeToCSV(ArrayList<String[]> wholeTableAsList) {
        try (FileOutputStream fos = new FileOutputStream(Main.csvPath);
             OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
             CSVWriter writer = new CSVWriter(osw)) {
            writer.writeAll(wholeTableAsList);
        } catch (IOException e) {
            System.out.println("Some problem happened while writing to the CSV file");
//            e.printStackTrace();
        }
    }

    // total operation is wrapped by this method
    public void scrape() {
        DataHandler dataHandler = new DataHandler();
        String rawHTML = dataHandler.fetchData();
        if (rawHTML == null) {
            System.out.println("No HTML Data Found. Exiting.");
        } else {
            ArrayList<String[]> wholeTableAsList = dataHandler.parseData(rawHTML);
            writeToCSV(wholeTableAsList);
            System.out.println("Data scraping successful.");
        }
    }
}
