import java.io.IOException;

public class Main {
    public static final String url = "https://finance.yahoo.com/quote/%5EVIX/history?p=%5EVIX";
    public static final String csvPath = "src/main/resources/CBOE.csv";

    public static void main(String[] args) throws IOException {
        Solution solution = new Solution();
        solution.scrape();
    }
}
