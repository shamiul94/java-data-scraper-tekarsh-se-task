# java-data-scraper-tekarsh-SE-task

## Assumptions

1. While making a get request by JSoup, `yahoo.finance` returns just 100 rows at a while although the table actually has
   a lot more than 100 rows which shows up upon scrolling further manually. This task probably could have been done
   using Selenium framework and a bit of javascript manipulation. I am assuming that this was not a part of this
   project, therefore, I am writing these 100 rows in the CSV file.

## How to run this project

File structure is -

```
.
│   Data-Scraper.iml
│   pom.xml
│   README.md
│
├───.idea
├───src
│   ├───main
│   │   ├───java
│   │   │       DataHandler.java
│   │   │       Main.java
│   │   │       Solution.java
│   │   │
│   │   └───resources
│   │           CBOE.csv
│   │
│   └───test
│       └───java
└───target
├───classes
└───generated-sources
└───annotations
```

1. This project was written in Intellij IDEA
2. All dependencies are managed using maven
3. Java SE 15 was used to code this project

## Where is the output?

The output will be located in the ``src/main/resources`` folder. One can change this path in the `Main.java` file.

## Caution

1. MS Excel shows '########' instead of the real value sometimes. Apparently the solution is to increase the cell width a bit. Then it shows the proper values.